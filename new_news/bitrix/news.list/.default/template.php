<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<!-- <?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('NEWS_DELETE_CONFIRM')));
	?>
	<div class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img class="preview_picture" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" style="float:left" /></a>
			<?else:?>
				<img class="preview_picture" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" style="float:left" />
			<?endif;?>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
			<div class="news-date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
		<?endif?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
				<div class="news-title"><?echo $arItem["NAME"]?></div>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<div class="news-detail"><?echo $arItem["PREVIEW_TEXT"];?></div>
		<?endif;?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<div style="clear:both"></div>
		<?endif?>
		<?foreach($arItem["FIELDS"] as $code=>$value):?>
			<small>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
			</small><br />
		<?endforeach;?>
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<small>
			<?=$arProperty["NAME"]?>:&nbsp;
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
				<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
			<?else:?>
				<?=$arProperty["DISPLAY_VALUE"];?>
			<?endif?>
			</small><br />
		<?endforeach;?>
		<a class="news-detail-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage('MEWS_DETAIL_LINK')?> &rarr;</a>
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div> -->

<!-- <pre>
	<?php
		print_r($arResult["ITEMS"]);
	?>
</pre> -->

<div class="new__news">
	<div class="new__news__left">
		<div class="new__news__left__headline">
			<h3><?=GetMessage('NEWS_LEFT_HEADLINE')?></h3>
		</div>
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?if($arItem["DISPLAY_PROPERTIES"]["MAIN_NEW"]["DISPLAY_VALUE"] == "Да"):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('NEWS_DELETE_CONFIRM')));
				?>
				<div class="new__news__left__block" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="new__news__left__block__link">
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
					</div>

					<div class="new__news__left__block__img">
						<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
					</div>
				</div>
			<?endif;?>
		<?endforeach;?>
	</div>
	
	<?$i = 0;?>
	<?$k = 0;?>
	<div class="new__news__right">
		<?if($i == 0):?>
			<div>
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?if($arItem["DISPLAY_PROPERTIES"]["MAIN_NEW"]["DISPLAY_VALUE"] != "Да"):?>
						<?if($i == 0):?>
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="new__news__right__first" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)">
								<div class="new__news__right__first__headline">
									<h3><?=$arItem["NAME"]?></h3>
								</div>

								<div class="new__news__right__first__text">
									<p><?echo $arItem["PREVIEW_TEXT"];?></p>
								</div>
								
								<div class="new__news__right__first__date">
									<p><?echo $arItem["TIMESTAMP_X"];?></p>
								</div>
							</a>
						<?endif;?>

						<?$i++;?>
						<?break;?>
					<?endif;?>
				<?endforeach;?>
			</div>
		<?endif;?>
		
		<?if($i != 0):?>
			<div>
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?if($arItem["DISPLAY_PROPERTIES"]["MAIN_NEW"]["DISPLAY_VALUE"] != "Да"):?>
						<?if($k == 0):?>
							<?$k++;?>
							<?continue;?>
						<?elseif($k == 1):?>
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="new__news__right__second">
								<div class="new__news__right__second__img">
									<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
								</div>

								<div class="new__news__right__second__content">
									<div class="new__news__right__second__content__headline">
										<h3><?=$arItem["NAME"]?></h3>
									</div>

									<div class="new__news__right__second__content__date">
										<p><?echo $arItem["TIMESTAMP_X"];?></p>
									</div>
								</div>
							</a>
							<?$k++;?>
						<?else:?>
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="new__news__right__other">
								<div class="new__news__right__other__headline">
									<h3><?=$arItem["NAME"]?></h3>
								</div>

								<div class="new__news__right__other__date">
									<p><?echo $arItem["TIMESTAMP_X"];?></p>
								</div>
							</a>
						<?endif;?>
					<?endif;?>
				<?endforeach;?>
			</div>
		<?endif;?>
	</div>
</div>
